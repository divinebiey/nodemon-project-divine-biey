/*import {status} from './status';*/

const http = require('http');
const nodemon = require('nodemon');
const {status} =  require('./status');

const server = http.createServer((req, res) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, PUT, OPTIONS');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type');
    res.setHeader('Content-Type', 'application/json');

    if (req.method === 'OPTIONS') {
        // Handle preflight requests
        res.writeHead(200);
        res.end();
        return;
    }
    if (req.method === 'GET') {
        if (req.url === '/red') {
            res.end(JSON.stringify({ red: status.red }));
        } else if (req.url === '/blue') {
            res.end(JSON.stringify({ blue: status.blue }));
        } else if (req.url === '/status') {
            res.end(JSON.stringify(status));
        } else {
            res.writeHead(404);
            res.end(JSON.stringify({ error: 'Not Found' }));
        }
    } else if (req.method === 'PUT') {
        if (req.url === '/red') {
            status.red += 1;
            res.end(JSON.stringify({ red: status.red }));
        } else if (req.url === '/blue') {
            status.blue += 1;
            res.end(JSON.stringify({ blue: status.blue }));
        } else {
            res.writeHead(404);
            res.end(JSON.stringify({ error: 'Not Found' }));
        }    
    } else {
        res.writeHead(405);
        res.end(JSON.stringify({ error: 'Method Not Allowed' }));
    }
   
const fs = require('fs');

let status = {};

// Lees het bestand './status' uit bij het opstarten van de webserver
try {
  const data = fs.readFileSync('./status');
  status = JSON.parse(data);
} catch (err) {
  console.error('Fout bij het lezen van het bestand:', err);
}

// Functie om de status bij te werken en op te slaan
function updateStatus(newStatus) {
  status = newStatus;

  // Sla de bijgewerkte status op in het './status' bestand
  fs.writeFile('./status', JSON.stringify(status), (err) => {
    if (err) {
      console.error('Fout bij het schrijven naar het bestand:', err);
    } else {
      console.log('Status succesvol opgeslagen.');
    }
  });
}
const newStatus = { red: 1, blue: 2 };
updateStatus(newStatus);

    res.end('test');


});


server.listen(3000, () => {
    console.log('Server is running on port 3000');
});

nodemon({
    script: 'index.js',
    ext:'js',
    ignore:['.nodemon-version']
}).on('restart', () => {
    console.log('Server restarted');
});


/*
De OPTIONS-methode en CORS (Cross-Origin Resource Sharing) zijn beide gerelateerd aan het mogelijk maken van veilige cross-origin verzoeken in webapplicaties.
De OPTIONS-methode is een HTTP-methode die wordt gebruikt als onderdeel van het CORS-mechanisme.
CORS daarentegen is een mechanisme dat servers in staat stelt om te specificeren wie toegang heeft tot hun bronnen.*/

